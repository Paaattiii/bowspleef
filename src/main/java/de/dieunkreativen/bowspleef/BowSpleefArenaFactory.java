package de.dieunkreativen.bowspleef;

import mc.alk.arena.objects.arenas.Arena;
import mc.alk.arena.objects.arenas.ArenaFactory;
import org.bukkit.plugin.Plugin;

/**
 * 
 * 
 * @author Nikolai
 */
public class BowSpleefArenaFactory implements ArenaFactory {
    
    Plugin plugin;
    
    public BowSpleefArenaFactory(Plugin reference) {
        this.plugin = reference;
    }

    public Arena newArena() {
        return new BowSpleefArena(plugin);
    }
    
}

