package de.dieunkreativen.bowspleef;

import mc.alk.arena.executors.CustomCommandExecutor;
import mc.alk.arena.executors.MCCommand;
import mc.alk.arena.objects.arenas.Arena;

import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class BowSpleefExecutor extends CustomCommandExecutor {

    @MCCommand(cmds = {"setLayer"}, admin = true)
    public boolean setLayer(Player sender, Arena arena) {
        return setLayer(sender, arena, 1);
    }

    @MCCommand(cmds = {"setLayer"}, admin = true, order = 1)
    public boolean setLayer(Player sender, Arena arena, Integer layerIndex) {
        try {
            BowSpleefArenaEditor sae = new BowSpleefArenaEditor(arena);
            sae.setLayer(sender, layerIndex);
            return sendMessage(sender, "&2BowSpleef Layer " + layerIndex + " has been created");
        } catch (BowSpleefException e) {
            return sendMessage(sender, e.getMessage());
        } catch (Exception e) {
            e.printStackTrace();
            return sendMessage(sender, ChatColor.RED + "Error creating region. " + e.getMessage());
        }
    }

    @MCCommand(cmds = {"setRegen"}, admin = true)
    public boolean setRegen(CommandSender sender, Arena arena, Integer regenTime) {
        return setRegen(sender, arena, 1, regenTime);
    }

    @MCCommand(cmds = {"setRegen"}, admin = true, order = 1)
    public boolean setRegen(CommandSender sender, Arena arena, Integer layerIndex, Integer regenTime) {
        try {
            BowSpleefArenaEditor sae = new BowSpleefArenaEditor(arena);
            sae.setRegen(layerIndex, regenTime);
            return sendMessage(sender, "&2BowSpleef Layer " + layerIndex + " now regens every " + regenTime + " seconds");
        } catch (BowSpleefException e) {
            return sendMessage(sender, e.getMessage());
        } catch (Exception e) {
            e.printStackTrace();
            return sendMessage(sender, ChatColor.RED + "Error setting regen. " + e.getMessage());
        }
    }

    @MCCommand(cmds = {"deleteRegen"}, admin = true)
    public boolean deleteRegen(CommandSender sender, Arena arena) {
        return deleteRegen(sender, arena, 1);
    }

    @MCCommand(cmds = {"deleteRegen"}, admin = true, order = 1)
    public boolean deleteRegen(CommandSender sender, Arena arena, Integer layerIndex) {
        try {
            BowSpleefArenaEditor sae = new BowSpleefArenaEditor(arena);
            sae.deleteRegen(layerIndex);
            return sendMessage(sender, "&2BowSpleef Layer " + layerIndex + " now no longer regens during the match");
        } catch (BowSpleefException e) {
            return sendMessage(sender, e.getMessage());
        } catch (Exception e) {
            e.printStackTrace();
            return sendMessage(sender, ChatColor.RED + "Error deleting regen. " + e.getMessage());
        }
    }
}
