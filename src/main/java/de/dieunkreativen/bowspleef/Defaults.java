package de.dieunkreativen.bowspleef;

public class Defaults {
    
    public static int MAX_LAYERS = 10;
    public static int MAX_REGION_SIZE = 20000;
    public static boolean HEIGHT_LOSS = false;
    
}
