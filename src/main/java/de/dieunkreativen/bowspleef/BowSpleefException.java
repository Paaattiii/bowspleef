package de.dieunkreativen.bowspleef;

public class BowSpleefException extends Exception {

    private static final long serialVersionUID = 1L;

    public BowSpleefException(String string) {
        super(string);
    }
}
