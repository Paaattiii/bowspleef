package de.dieunkreativen.bowspleef;

import mc.alk.arena.BattleArena;
import mc.alk.arena.objects.arenas.ArenaFactory;
import mc.alk.arena.util.Log;
import de.dieunkreativen.bowspleef.BowSpleefExecutor;
import org.bukkit.Bukkit;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.plugin.Plugin;
import org.bukkit.plugin.java.JavaPlugin;

public class BowSpleef extends JavaPlugin {
    
    static BowSpleef plugin;  /// our plugin

    @Override
    public void onEnable() {
        plugin = this;
        // ArenaSpleef requires BattleArena, WorldEdit, WorldGuard:
        Plugin ba = Bukkit.getPluginManager().getPlugin("BattleArena");
        Plugin we = Bukkit.getPluginManager().getPlugin("WorldEdit");
        Plugin wg = Bukkit.getPluginManager().getPlugin("WorldGuard");
        if (!ba.isEnabled() || !we.isEnabled() || !wg.isEnabled()) {
            Log.err("BowSpleef needs BattleArena, WorldEdit, and WorldGuard.");
            Log.info("disabling BowSpleef");
            setEnabled(false);
            return;
        }

        /// Register our spleef type
        ArenaFactory factory = new BowSpleefArenaFactory(this);
        BattleArena.registerCompetition(this, "BowSpleef", "bowspleef", factory, new BowSpleefExecutor());

        /// Load the Config
        loadConfig();
        Log.info("[" + getName() + "] v" + getDescription().getVersion() + " enabled!");
    }

    @Override
    public void onDisable() {
        Log.info("[" + getName() + "] v" + getDescription().getVersion() + " stopping!");
    }

    @Override
    public void reloadConfig() {
        super.reloadConfig();
        loadConfig();
    }

    public static BowSpleef getSelf() {
        return plugin;
    }

    public void loadConfig() {
        /// create our default config if it doesn't exist
        saveDefaultConfig();

        /// Read in our default spleef options
        FileConfiguration config = this.getConfig();
        Defaults.MAX_LAYERS = config.getInt("maxLayers", Defaults.MAX_LAYERS);
        Defaults.MAX_REGION_SIZE = config.getInt("maxRegionSize", Defaults.MAX_REGION_SIZE);
        Defaults.HEIGHT_LOSS = config.getBoolean("heightLoss", Defaults.HEIGHT_LOSS);

        if (config.getBoolean("islanding.enable", true)) {
            BowSpleefArena.ISLAND_FAILS = config.getInt("islanding.fails", BowSpleefArena.ISLAND_FAILS);
            BowSpleefArena.ISLAND_RADIUS = config.getInt("islanding.radius", BowSpleefArena.ISLAND_RADIUS);
        } else {
            BowSpleefArena.ISLAND_FAILS = -1;
        }

    }
}

